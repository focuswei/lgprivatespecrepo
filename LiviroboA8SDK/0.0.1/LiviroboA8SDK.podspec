
Pod::Spec.new do |s|
  s.name             = 'LiviroboA8SDK'
  s.version          = '0.0.1'
  s.summary          = 'LiviroboA8SDK for A8Robot'

  s.homepage         = 'https://livirobo.coding.net/public/ios/LiviRoboSpecs/git/files'
  s.license          = 'none'
  s.author           = { 'focuswei' => 'w394966935@gmail.com' }
  s.platform         = :ios, "12.0"
  s.source           = { :git => 'https://e.coding.net/livirobo/ios/LiviroboDeviceA8Kit.git', :tag => s.version.to_s }
  s.swift_version     = "5.0"
  s.resource          = 'LiviroboA8SDK.framework/LGA8ToolResource.bundle'
  s.framework         = "UIKit","CoreGraphics"
  s.vendored_frameworks = 'LiviroboA8SDK.framework'
  s.dependency 'TuyaSmartHomeKit'
  s.dependency 'TuyaSmartSweeperKit'
  s.dependency 'SnapKit'
  s.dependency 'Kingfisher'
  s.dependency 'CryptoSwift'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
