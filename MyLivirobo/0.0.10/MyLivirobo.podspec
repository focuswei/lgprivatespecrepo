
Pod::Spec.new do |spec|

  spec.name          = "MyLivirobo"
  spec.version       = "0.0.10"
  spec.summary       = "This project help livirobo simple framework"

  spec.homepage      = "https://gitlab.com/focuswei/mylivirobo"

  spec.license       = { :type => 'MIT', :file => 'LICENSE' }


  spec.author        = { 'focuswei' => 'w394966935@gmail.com' }
 
  spec.platform      = :ios, "12.0"


  spec.source        = { :git => "https://gitlab.com/focuswei/mylivirobo.git", :tag => "#{spec.version}" }
  
  spec.swift_version = "5.0"
  spec.framework     = "UIKit","Foundation","CoreGraphics"
  spec.ios.vendored_frameworks = "build/livirobo.xcframework"
end
