

Pod::Spec.new do |s|
  s.name             = 'LGLikeArtsyProject'
  s.version          = '0.1.3'
  s.summary          = 'This is ObjC Framework , name is LGLikeArtsyProject.'


  s.homepage         = 'https://gitlab.com/focuswei/like_artsy_project'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'focuswei' => 'w394966935@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/focuswei/like_artsy_project.git', :tag => s.version }

  s.ios.deployment_target = '12.0'

  s.vendored_library = 'Build/LGLikeArtsyProject.framework'
  s.source_files = 'LGLikeArtsyProject/Classes/**/*'
end
