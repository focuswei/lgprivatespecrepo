Pod::Spec.new do |spec|

  spec.name         = "LGPrivateRemoteDemo"
  spec.version      = "0.0.2"
  spec.summary      = "This project private"

  spec.homepage     = "https://gitlab.com/focuswei/lgprivateremotedemo"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }


  spec.author       = { 'focuswei' => 'w394966935@gmail.com' }
 
  spec.platform     = :ios, "12.0"


  spec.source       = { :git => "https://gitlab.com/focuswei/lgprivateremotedemo.git", :tag => "#{spec.version}" }

  spec.source_files  = "privateRemoteDemo", "privateRemoteDemo/*.swift"
  spec.swift_version = "5.0"
  spec.framework     = "UIKit","Foundation"
  
end
