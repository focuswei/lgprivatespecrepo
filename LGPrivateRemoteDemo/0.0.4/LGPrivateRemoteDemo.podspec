Pod::Spec.new do |spec|

  spec.name         = "LGPrivateRemoteDemo"
  spec.version      = "0.0.4"
  spec.summary      = "This project private"

  spec.homepage     = "https://gitlab.com/focuswei/lgprivateremotedemo"

  spec.author       = { 'focuswei' => 'w394966935@gmail.com' }
 
  spec.platform     = :ios, "12.0"


  spec.source       = { :git => "https://gitlab.com/focuswei/lgprivateremotedemo.git", :tag => "#{spec.version}" }

  spec.source_files  = "privateRemoteDemo", "privateRemoteDemo/*.swift"
  spec.resource = 'privateRemoteDemo/Resource_bundle/LGA8ToolResource.bundle'
  spec.swift_version = "5.0"
  spec.framework     = "UIKit","Foundation","CoreGraphics"
  spec.dependency 'TuyaSmartHomeKit'
  spec.dependency 'TuyaSmartSweeperKit'
  spec.dependency 'SnapKit'
  spec.dependency 'Kingfisher'
  spec.dependency 'Alamofire'
  spec.dependency 'CryptoSwift'
  spec.static_framework = true
end
